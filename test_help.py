import pytest
from unittest.mock import patch, MagicMock
import bot

@pytest.fixture
def mock_bot():
    with patch('bot.bot') as mock:
        yield mock

def test_help_command(mock_bot):
    message = MagicMock()
    message.text = '/help'
    message.chat.id = 12345

    bot.send_help(message)

    mock_bot.reply_to.assert_called_once_with(message,
        "Отправь мне фотографию, и я отмечу линии на теле человека на ней.")
    
if __name__ == '__main__':
    test_help_command()