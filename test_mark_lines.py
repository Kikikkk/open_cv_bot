import cv2
import numpy as np
import body

TEST_IMAGE_PATH = 'images/test.jpg'
EXPECTED_IMAGE_PATH = 'images/expected.jpg'

def test_markLines():
    input_image = cv2.imread(TEST_IMAGE_PATH)
    
    processed_image = body.markLines(input_image)
    
    expected_image = cv2.imread(EXPECTED_IMAGE_PATH)
    
    difference = cv2.absdiff(processed_image, expected_image)
    mean_diff = np.mean(difference)
    assert mean_diff < 5, "Среднее абсолютное отличие между изображениями слишком велико: {}".format(mean_diff)

if __name__ == '__main__':
    test_markLines()
