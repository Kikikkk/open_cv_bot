# Use the official Python image from the Docker Hub
FROM python:3.9-slim

# Install system dependencies
RUN apt-get update && apt-get install -y \
    libgl1-mesa-glx \
    libglib2.0-0

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy the requirements file into the container
COPY requirements.txt ./

# Install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the rest of the application code into the container
COPY . .

# Copy the test script and set execute permission
COPY run_tests.sh ./
RUN chmod +x run_tests.sh

# Expose the port the bot will run on
EXPOSE 8443

# Set the default command to run the bot
CMD ["python", "bot.py"]
