import pytest
from unittest.mock import patch, MagicMock
import bot

@pytest.fixture
def mock_bot():
    with patch('bot.bot') as mock:
        yield mock

def test_start_command(mock_bot):
    message = MagicMock()
    message.text = '/start'
    message.chat.id = 12345

    bot.send_welcome(message)

    mock_bot.reply_to.assert_called_once_with(message,
        "Привет! Я бот, который может отметить линии на теле человека на фотографии. "
        "Просто отправь мне фотографию и я сделаю все остальное!")

if __name__ == '__main__':
    test_start_command()