import telebot
from io import BytesIO
from PIL import Image
import body
import numpy as np

# токен вашего бота
TOKEN = '6011041873:AAHA02vnvYE4KhrvfqLqHCWjGklFj1FhiWA'

# Создаем объект бота
bot = telebot.TeleBot(TOKEN)

# Обработчик команды /start
@bot.message_handler(commands=['start'])
def send_welcome(message):
    bot.reply_to(message,
                 "Привет! Я бот, который может отметить линии на теле человека на фотографии. "
                 "Просто отправь мне фотографию и я сделаю все остальное!")

# Обработчик команды /help
@bot.message_handler(commands=['help'])
def send_help(message):
    bot.reply_to(message, "Отправь мне фотографию, и я отмечу линии на теле человека на ней.")

# Обработчик всех остальных сообщений с фотографией
@bot.message_handler(content_types=['photo'])
def process_photo(message):
    file_id = message.photo[-1].file_id

    # Получаем объект файла фотографии
    file_info = bot.get_file(file_id)

    # Скачиваем файл фотографии в память
    file = bot.download_file(file_info.file_path)
    img = Image.open(BytesIO(file))

    # Преобразуем изображение и добавляем отметки на теле
    annotated_img = body.markLines(np.array(img))

    # Сохраняем изображение в памяти в формате JPEG
    output = BytesIO()
    Image.fromarray(annotated_img).save(output, format='JPEG')

    # Отправляем измененное изображение обратно пользователю
    bot.send_photo(message.chat.id, photo=output.getvalue())


# Запускаем бота
bot.polling()
