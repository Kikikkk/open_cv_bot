import cv2
import mediapipe as mp

def markLines(img):
    face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
    mp_drawing = mp.solutions.drawing_utils
    mp_pose = mp.solutions.pose
    faces = face_cascade.detectMultiScale(img, 1.3, 5)
    with mp_pose.Pose(
            static_image_mode=True, min_detection_confidence=0.5, model_complexity=2
    ) as pose:
        image_rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        results = pose.process(image_rgb)

        # рисуем линии на теле
        annotated_image = img.copy()
        mp_drawing.draw_landmarks(
            annotated_image, results.pose_landmarks, mp_pose.POSE_CONNECTIONS
        )

        return annotated_image
